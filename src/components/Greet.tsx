// type GreetProps = {
//   name: string;
//   messageCount: number;
//   isLoggedIn: boolean;
// };

type GreetProps = {
  name: string;
  messageCount?: number;
  isLoggedIn: boolean;
};

const Greet = (props: GreetProps) => {
  const { messageCount = 0 } = props;
  return (
    <div>
      <h2>
        {props.isLoggedIn
          ? ` Welcome ${props.name} ! you have ${props.messageCount} unread messages.`
          : "welcome Guest"}
      </h2>
    </div>
  );
};

export default Greet;
