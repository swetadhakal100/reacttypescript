import { useState } from "react";

type Authuser = {
  name: string;
  email: string;
};

export const User = () => {
  // const [user, setUser] = useState<Authuser | null>(null);
  const [user, setUser] = useState<Authuser>({} as Authuser);

  const handleLogin = () => {
    setUser({
      name: "sweta",
      email: "swetadhakal100@gmail.com",
    });
  };

  // const handleLogout = () => {
  //   setUser(null);
  // };

  return (
    <div>
      <button onClick={handleLogin}>Login</button>
      {/* <button onClick={handleLogout}>Logout</button> */}
      {/* <div>your name is {user?.name}</div>
      <div>your email is{user?.email}</div> */}
      <div>your name is {user.name}</div>
      <div>your email is{user.email}</div>
    </div>
  );
};
