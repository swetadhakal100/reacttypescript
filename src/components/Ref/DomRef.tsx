import { useEffect, useRef } from "react";
export const DomRef = () => {
  //   const InputRef = useRef<HTMLInputElement>(null);
  const InputRef = useRef<HTMLInputElement>(null!);

  useEffect(() => {
    // InputRef.current?.focus();
    InputRef.current.focus();
  }, []);

  return (
    <div>
      <input ref={InputRef} type="text" />
    </div>
  );
};
