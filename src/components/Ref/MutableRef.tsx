import { useState, useEffect, useRef } from "react";

export const MutableRef = () => {
  const [timer, setTimer] = useState(0);
  //   const interValRef = useRef<number | undefined>(undefined);
  const interValRef = useRef<number | null>(null);

  const stopTimer = () => {
    // window.clearInterval(interValRef.current);
    if (interValRef.current) window.clearInterval(interValRef.current);
  };

  useEffect(() => {
    console.log("i am useEffect");
    interValRef.current = window.setInterval(() => {
      setTimer((timer) => timer + 1);
    }, 1000);
    return () => {
      console.log("I am clean up function");
      stopTimer();
    };
  }, []);

  return (
    <div>
      Hook Timer - {timer}
      <button onClick={() => stopTimer()}>Stop Timer</button>
    </div>
  );
};
