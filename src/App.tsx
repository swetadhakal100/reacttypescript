import "./App.css";
import { Text } from "./components/polymorphic/Text";
// import { CustomButton } from "./components/home/Button";
// import { Counter } from "./components/Class/Counter";
// import { Private } from "./components/auth/Private";
// import { Profile } from "./components/auth/Profile";
// import { List } from "./components/generics/List";
// import { RandomNumber } from "./components/restriction/RandomNumber";
// import { Toast } from "./components/templateLiterals/Toast";
// import { Button } from "./components/Button";
// import { Container } from "./components/Container";
// import { Input } from "./components/Input";
// import { Heading } from "./components/Heading";
// import { Oscar } from "./components/Oscar";
// import Greet from "./components/Greet";
// import { Person } from "./components/Person";
// import { PersonList } from "./components/PersonList";
// import { Status } from "./components/Status";
// import { ThemeContextProvider } from "./components/Context/ThemeContext";
// import { Box } from "./components/Context/Box";
// import { UserContextProvider } from "./components/Context/UserContext";
// import { MutableRef } from "./components/Ref/MutableRef";
// import { User } from "./components/State/User";
function App() {
  const personName = {
    first: "switu",
    last: "dhakal",
  };

  const nameList = [
    {
      first: "shiri",
      last: "xettri",
    },
    {
      first: "nihu",
      last: "ghimire",
    },
    {
      first: "switu",
      last: "dahal",
    },
    {
      first: "princess",
      last: "Diana",
    },
  ];

  return (
    <div className="App">
      <Text as="h1" size="lg">
        Heading
      </Text>
      <Text as="p" size="md">
        Paragraph
      </Text>
      <Text as="label" htmlFor="someId" size="sm" color="primary">
        Label
      </Text>

      {/* <CustomButton varient="primary" onClick={() => console.log("clicked")}>
        Button Clicked
      </CustomButton> */}

      {/* <Toast position="center" /> */}

      {/* <RandomNumber value={10} isPositive /> */}
      {/* <List
        items={["sweta", "neha", "switu"]}
        onClick={(item) => console.log(item)}
      />
      <List items={[1, 2, 3]} onClick={(item) => item} /> */}

      {/* <List
        items={[
          {
            id: 1,
            first: "shiri",
            last: "xettri",
          },
          {
            id: 2,
            first: "nihu",
            last: "ghimire",
          },
          {
            id: 3,
            first: "switu",
            last: "dahal",
          },
          {
            id: 4,
            first: "princess",
            last: "Diana",
          },
        ]}
        onClick={(item) => console.log(item)}
      /> */}

      {/* <Private isLoggedIn={true} component={Profile} /> */}

      {/* <Counter message="The count value is " /> */}

      {/* <MutableRef /> */}

      {/* <UserContextProvider>
        <User />
      </UserContextProvider> */}

      {/* <ThemeContextProvider>
        <Box />
      </ThemeContextProvider> */}

      {/* <Container styles={{ border: "1px solid black", padding: "1rem" }} /> */}

      {/* <Button
        handleClick={(event, id) => {
          console.log("button clicked", event, id);
        }}
      />
      <Input value="" handleChange={(event) => console.log(event)} /> */}
      {/* <Status status="loading" />
      <Heading>Placeholder text</Heading>
      <Oscar>
        <Heading>Oscar goes to Leonardo Dicpario! </Heading>
      </Oscar> */}
      {/* <Greet name="sweta" messageCount={20} isLoggedIn={false} /> */}
      {/* <Greet name="sweta" isLoggedIn={false} /> */}
      {/* <Person name={personName} />
      <PersonList names={nameList} /> */}
    </div>
  );
}

export default App;
